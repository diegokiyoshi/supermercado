package br.diego;

import java.util.List;

public class Produto {
	
	private String nome;
	private Double preco;
	private Integer quantidade;
	private Categoria categoria;
	private static Integer id = 0;
	private Integer idProduto;
	
	public Produto() {
		id++;
		setIdProduto(id);
	}
	
	public Integer getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(Integer idProduto) {
		this.idProduto = idProduto;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public Integer getQuantidade() {
		return quantidade;
	}
	
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	
	public Categoria getCategoria() {
		return categoria;
	}
	
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	
	
	public static void exibirProdutos(List<Produto> listaProduto) {
		
		for (Produto produto : listaProduto) {
			
			System.out.println("ID do produto: " + produto.getIdProduto());
			System.out.println("Nome do Produto: " + produto.getNome());
			System.out.println("Pre�o: " + produto.getPreco());
			System.out.println("Quantidade de estoque: " + produto.getQuantidade());
			System.out.println("Categoria: "  + produto.getCategoria().getCategoriaProduto() + "\n");
			
		}
		
	}

}
