package br.diego;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Cadastro {

	public static List<Produto> cadastrarProduto(List<Autenticavel> listaAutenticavel){
		
		List<Produto> listaProduto = new ArrayList<>();
		Scanner scanner = new Scanner(System.in);
		Boolean voltaLogin = Boolean.TRUE;
		
		while(voltaLogin) {
			
			System.out.println("Digite seu login:");
			String cpf = scanner.next();
			
			System.out.println("Digite sua senha: ");
			Integer senha = scanner.nextInt();
			
			if(.autenticar(cpf, senha, listaAutenticavel)) {
				
				Boolean desejaCadastrar = Boolean.TRUE;
				String produtoOuSair;

				while (desejaCadastrar) {
					
					System.out.println("Ol�, deseja cadastrar um produto? Escreva o'nome, pre�o, quantidade de "
							+ "estoque e categoria ou digite 'SAIR' para encerrar.");
					produtoOuSair = scanner.next();

					if (!produtoOuSair.equalsIgnoreCase("Sair")) {
					
						String[]retornaSplit = produtoOuSair.split(",");
						Produto produto = new Produto();
						Categoria categoria = new Categoria();
						
						
						produto.setNome(retornaSplit[0]);
						
						Double precoProduto = Double.parseDouble(retornaSplit[1]);
						produto.setPreco(precoProduto);
						
						Integer quantidadeProduto = Integer.parseInt(retornaSplit[2]);
						produto.setQuantidade(quantidadeProduto);
						
						produto.setCategoria(categoria);
						
//						Categoria categoriaDoProdutoCadastrado = produto.getCategoria();
//						
//						categoriaDoProdutoCadastrado.setCategoriaProduto(retornaSplit[3]);
						
						produto.getCategoria().setCategoriaProduto(retornaSplit[3]);
						
						listaProduto.add(produto);
						
						System.out.println("Produto cadastrado com sucesso");
						
					} else {
						desejaCadastrar = Boolean.FALSE;
					}
				}
			} else {
				System.out.println("Seu login ou senha est�o incorretos");
			}
		}
		
		
		return listaProduto;
		
		
	}
	
	public static List<Funcionario> cadastraFuncionario() {

		List<Funcionario> listaFuncionario = new ArrayList<Funcionario>();
		Scanner scan = new Scanner(System.in);
		String opcaoCadastro;
		Boolean desejaCadastrarFuncionario = Boolean.TRUE;

		while (desejaCadastrarFuncionario) {

			System.out.println("Deseja cadastrar um funcion�rio? 'SIM' ou 'NAO' ");
			opcaoCadastro = scan.nextLine();

			if (opcaoCadastro.equalsIgnoreCase("SIM")) {
				Funcionario funcionario = new Funcionario();
				System.out.println("Digite o nome do funcion�rio");
				String nome = scan.nextLine();
				funcionario.setNome(nome);

				System.out.println("Digite o CPF do funcionario");
				String cpf = scan.nextLine();
				funcionario.setCpf(cpf);

				System.out.println("Digite a senha do funcion�rio");
				Integer senha = scan.nextInt();
				funcionario.setSenha(senha);

				listaFuncionario.add(funcionario);

			} else if (opcaoCadastro.equalsIgnoreCase("NAO")) {
				desejaCadastrarFuncionario = Boolean.FALSE;
				// exibeFuncionarios(listaFuncionario);
			}

		}

		return listaFuncionario;
	}
}
