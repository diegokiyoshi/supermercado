package br.diego;


import java.util.ArrayList;
import java.util.List;

public class Funcionario implements Autenticavel{
	
	private String nome;
	private String cpf;
	private Integer senha;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public Integer getSenha() {
		return senha;
	}
	public void setSenha(Integer senha) {
		this.senha = senha;
	}
	
	 
	
	public static void exibirFuncionarios(List<Funcionario> listaFuncionario){
		
		for (Funcionario funcionario : listaFuncionario) {
			System.out.println(funcionario.getNome());
			System.out.println(funcionario.getCpf());
			System.out.println(funcionario.getSenha() + "\n");
		}
	}
	@Override
	public Boolean autenticar(String cpf, Integer senha, List<Autenticavel> listaAutenticavel) {
		// TODO Auto-generated method stub
		return null;
	}
}
