package br.diego;

import java.util.List;

public interface Autenticavel {
	
	void setSenha(Integer senha);
	
	void setCpf(String cpf);
	
	Integer getSenha();
	
	String getCpf();
	
	public Boolean autenticar(String cpf, Integer senha, List<Autenticavel> listaAutenticavel);

}
